﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;

namespace CMS
{
    public partial class Editpage : System.Web.UI.Page
    {
        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            DataRowView pagerow = getPageInfo(pageid);
            if (pagerow == null)
            {
               heading.InnerHtml = "No Page Found.";
                return;
            }
            heading.InnerHtml = "Editing " + pagerow["page_title"];
            pageTitle.Text = pagerow["page_title"].ToString();
            pageAuthor.Text = pagerow["page_author"].ToString();
            pageContent1.Text = pagerow["page_content1"].ToString();
            pageContent2.Text = pagerow["page_content2"].ToString();
            publishStatus.SelectedValue = pagerow["page_publishedStatus"].ToString();
        }
        protected void EditPage(object sender, EventArgs e)
        {
            string title = pageTitle.Text;
            string author = pageAuthor.Text;
            string content1 = pageContent1.Text;
            string content2 = pageContent1.Text;
            string status = publishStatus.SelectedValue.ToString();

            string editquery = "Update PAGES set page_title='" + title + "'," +
                "page_author='" + author + "',page_content1='" + content1 + "',"+
                "page_content2='"+content2+"',page_publishedStatus='"+status+ "'"+
                 "where page_id=" + pageid;

            page_query.UpdateCommand = editquery;
            page_query.Update();
            result.InnerHtml= result.InnerHtml = "<div class='alert alert-success' role='alert'>Success! Page status has been updated. </div><br><a href='PageManager.aspx'>";
        }
        protected DataRowView getPageInfo(int id)
        {
            string query = "select * from PAGES where page_id=" + pageid.ToString();
            page_query.SelectCommand = query;
            
            DataView pageview = (DataView)page_query.Select(DataSourceSelectArguments.Empty);

            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0];
            return pagerowview;

        }
    }
}