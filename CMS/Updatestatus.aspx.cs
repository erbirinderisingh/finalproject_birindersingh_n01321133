﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMS
{
    public partial class Updatestatus : System.Web.UI.Page
    {
        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }
        public string status
        {
            get { return Request.QueryString["status"]; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            string query = "update PAGES set page_publishedstatus ='"+status.ToString()+"' WHERE page_id=" + pageid;
            page_query.UpdateCommand = query;
            page_query.Update();
            result.InnerHtml = "<div class='alert alert-success' role='alert'>Success! Page status has been updated. </div><br><a href='PageManager.aspx'><span class='btn btn-primary'>Back</span></a>";
        }
    }
}