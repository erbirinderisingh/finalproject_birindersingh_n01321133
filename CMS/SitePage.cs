﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS
{
    public class SitePage
    {
        private string pageTitle;
        private string pageAuthor;
        private string pageContent1;
        private string pageContent2;
        private string pageStatus;

        public string PageTitle
        {
            get { return pageTitle; }
            set { pageTitle = value; }
        }
        public string PageAuthor
        {
            get { return pageAuthor; }
            set { pageAuthor = value; }
        }
        public string PageContent1
        {
            get { return pageContent1; }
            set { pageContent1 = value; }
        }
        public string PageContent2
        {
            get { return pageContent2; }
            set { pageContent2 = value; }
        }
        public string PageStatus
        {
            get { return pageStatus; }
            set { pageStatus = value; }
        }

    }
}