﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Pageshow.aspx.cs" Inherits="CMS.Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <asp:SqlDataSource runat="server" id="page_query1" ConnectionString="<%$ ConnectionStrings:cms_sql_con %>">
    </asp:SqlDataSource>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <h1 runat="server" ID="heading"></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <em>Author:<span runat="server" ID="author"></span></em>
        </div>
        <div class="col-lg-6 col-md-6">
            <em>Page Created:<span runat="server" ID="createdOn"></span></em>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <hr/>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-6 col-md-6" runat="server" ID="section1">
        
        </div>
        <div class="col-lg-6 col-md-6" runat="server" ID="section2">
        
        </div>
    </div>
   
</asp:Content>

