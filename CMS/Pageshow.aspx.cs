﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMS
{
    public partial class Page : System.Web.UI.Page
    {
        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            base.OnPreRender(e);
            DataRowView pagerow = getPageInfo1(pageid);
            if (pagerow == null)
            {
                heading.InnerHtml = "No Page Found.";
                return;
            }
            heading.InnerHtml = pagerow["page_title"].ToString();
            section1.InnerHtml = pagerow["page_content1"].ToString();
            section2.InnerHtml = pagerow["page_content2"].ToString();
            author.InnerHtml = pagerow["page_author"].ToString();
            createdOn.InnerHtml = pagerow["created_date"].ToString();

        }
        protected override void OnPreRender(EventArgs e)
        {
            //base.OnPreRender(e);
            //DataRowView pagerow = getPageInfo1(pageid);
            //if (pagerow == null)
            //{
            //    heading.InnerHtml = "No Page Found.";
            //    return;
            //}
            //heading.InnerHtml = pagerow["page_title"].ToString();
            //section1.InnerHtml = pagerow["page_content1"].ToString();
            //section2.InnerHtml = pagerow["page_content2"].ToString();
            //author.InnerHtml = pagerow["page_author"].ToString();
            //createdOn.InnerHtml = pagerow["created_date"].ToString();
        }
        protected DataRowView getPageInfo1(int id)
        {
            string query = "select * from PAGES where page_id=" + pageid.ToString();
            page_query1.SelectCommand = query;
            DataView pageview = (DataView)page_query1.Select(DataSourceSelectArguments.Empty);

            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0];
            return pagerowview;
        }
    }
}