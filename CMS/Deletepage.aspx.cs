﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMS
{
    public partial class Deletepage : System.Web.UI.Page
    {
        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            string query = "update PAGES set is_deleted =1 WHERE page_id=" + pageid.ToString();
            page_query.UpdateCommand = query;
            page_query.Update();
            result.InnerHtml = "<div class='alert alert-success' role='alert'>Success! Page has been deleted. </div><br><a href='PageManager.aspx'><span class='btn btn-primary'>Back</span></a>";
        }
    }
}