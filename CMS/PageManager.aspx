﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PageManager.aspx.cs" Inherits="CMS.PageManager" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <asp:SqlDataSource runat="server" id="page_query" ConnectionString="<%$ ConnectionStrings:cms_sql_con %>">
    </asp:SqlDataSource>

    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createNewPageModal">
        Create New Page
    </button>
    <div class="modal fade" id="createNewPageModal" tabindex="-1" role="dialog" aria-labelledby="createNewPageModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="createNewPageModalLabel">Create New Page</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <%--<form id="formPageCreate" runat="server">--%>
                    <asp:Label runat="server">Page Title</asp:Label>
                    <asp:TextBox runat="server" CssClass="form-control" ID="pageTitle" placeholder="Page Title"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter Page Title" ControlToValidate="pageTitle" ID="validatorPageTitle"></asp:RequiredFieldValidator>
                    <br/>
                     <asp:Label runat="server">Author Name</asp:Label>
                    <asp:TextBox runat="server" CssClass="form-control" ID="pageAuthor" placeholder="Page Author"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter Page Author" ControlToValidate="pageAuthor" ID="validatorPageAuthor"></asp:RequiredFieldValidator>
                    <br/>
                     <asp:Label runat="server">Page Content 1</asp:Label>
                    <asp:TextBox runat="server" CssClass="form-control" ID="pageContent1" TextMode="multiline" Columns="50" Rows="5" placeholder="Page Content-1"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter Page Content 1" ControlToValidate="pageContent1" ID="validatorPageContent1"></asp:RequiredFieldValidator>
                    <br/>
                     <asp:Label runat="server">Page Content 2</asp:Label>
                    <asp:TextBox runat="server" CssClass="form-control" ID="pageContent2" TextMode="multiline" Columns="50" Rows="5" placeholder="Page Content-2"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter Page Content 2" ControlToValidate="pageContent2" ID="validatorPageContent2"></asp:RequiredFieldValidator>
                    <br />
                     <asp:Label runat="server">Publish Status</asp:Label>
                    <asp:DropDownList runat="server" CssClass="form-control" ID="publishStatus">
                        <asp:ListItem Value="P" Text="Published"></asp:ListItem>
                        <asp:ListItem Value="U" Text="UnPublished"></asp:ListItem>
                    </asp:DropDownList>
                  <%--  </form>--%>
                </div>
                <div class="modal-footer">
                      <asp:Button runat="server" CssClass="btn btn-secondary" ID="cancelButton" Text="Close" data-dismiss="modal"/>
                      <asp:Button runat="server" CssClass="btn btn-primary" ID="submitButton" OnClick="AddPage" Text="Submit"/>
                </div>
            </div>
        </div>
    </div>

    <asp:DataGrid ID="page_list" runat="server"></asp:DataGrid>
 

</asp:Content>
