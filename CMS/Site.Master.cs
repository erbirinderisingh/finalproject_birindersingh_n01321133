﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMS
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            DataView pages = getPageInfo();
           
            if (pages == null)
            {
                return;
            }
            foreach (DataRowView rowView in pages)
            {
                DataRow row = rowView.Row;
                navi.InnerHtml += "<li><a runat='server' href='/Pageshow.aspx?pageid=" + row["page_id"] + "'>" + row["page_title"] + "</a></li>";
            }

        }
        protected DataView getPageInfo()
        {
            string query = "select * from PAGES where page_publishedStatus='P'";
            page_query4.SelectCommand = query;

            DataView pageview = (DataView)page_query4.Select(DataSourceSelectArguments.Empty);

            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            return pageview;

        }
    }
}