﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMS
{
    public partial class PageManager : System.Web.UI.Page
    {
        private string insertNewPageQuery = "INSERT INTO PAGES " +
            "(page_title,page_author,page_content1,page_content2,page_publishedStatus,created_date,modified_date,is_deleted) VALUES";
        private string selectAllPagesQuery = "SELECT * FROM PAGES WHERE IS_DELETED=0";
        
        protected void Page_Load(object sender, EventArgs e)
        {
            page_query.SelectCommand = selectAllPagesQuery;
            page_list.DataSource = Pages_Manual_Bind(page_query);

            //BoundColumn pageId = new BoundColumn();
            //pageId.DataField = "page_id";
            //pageId.HeaderText = "ID";
            //pageId.Visible = true;
            //page_list.Columns.Add(pageId);

            //BoundColumn title = new BoundColumn();
            //title.DataField = "page_title";
            //title.HeaderText = "Page Title";
            //page_list.Columns.Add(title);

            //BoundColumn author = new BoundColumn();
            //author.DataField = "page_author";
            //author.HeaderText = "Author";
            //page_list.Columns.Add(author);

            //BoundColumn isPublished = new BoundColumn();
            //isPublished.DataField = "page_publishedStatus";
            //isPublished.HeaderText = "Status";
            //page_list.Columns.Add(isPublished);

            //BoundColumn created = new BoundColumn();
            //created.DataField = "created_date";
            //created.HeaderText = "Created";
            //page_list.Columns.Add(created);

            //BoundColumn modified = new BoundColumn();
            //modified.DataField = "modified_date";
            //modified.HeaderText = "Modified";
            //page_list.Columns.Add(modified);

            //ButtonColumn editPage = new ButtonColumn();
            //editPage.HeaderText = "Edit";
            //editPage.Text = "Edit";
            //editPage.ButtonType = ButtonColumnType.PushButton;
            //editPage.CommandName = "EditPage";
            //page_list.Columns.Add(editPage);

            //ButtonColumn delPage = new ButtonColumn();
            //delPage.HeaderText = "Delete";
            //delPage.Text = "Delete";
            //delPage.ButtonType = ButtonColumnType.PushButton;
            //delPage.CommandName = "DelStudent";
            //page_list.Columns.Add(delPage);

            page_list.CssClass = "table table-striped";
            page_list.DataBind();


        }
        protected DataView Pages_Manual_Bind(SqlDataSource src)
        { 
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
           

            DataColumn editcol = new DataColumn();
            editcol.ColumnName = "Edit";
            mytbl.Columns.Add(editcol);

            DataColumn delcol = new DataColumn();
            delcol.ColumnName = "Delete";
            mytbl.Columns.Add(delcol);

            mytbl.Columns["page_title"].ColumnName = "Page Title";
            mytbl.Columns["page_id"].ColumnName = "ID";
            mytbl.Columns["page_author"].ColumnName = "Author";
            mytbl.Columns["created_date"].ColumnName = "Created On";
            mytbl.Columns["modified_date"].ColumnName = "Modified On";
            mytbl.Columns["page_publishedStatus"].ColumnName = "Status";



            foreach (DataRow row in mytbl.Rows)
            {
                //Intercept a specific column rendering
                //add a link of that column info
                row["Page Title"] =
                    "<a href=\"Pageshow.aspx?pageid="
                    + row["ID"]
                    + "\">"
                    + row["Page Title"]
                    + "</a>";

                if (row["Status"].ToString() == "P")
                {
                    row["Status"] = "<a href=\"Updatestatus.aspx?pageid=" + row["ID"] + "&status=U"+"\"><img src='Images/green.png'></a>";
                }
                else {
                    row["Status"] = "<a href=\"Updatestatus.aspx?pageid=" + row["ID"] + "&status=P"+"\"><img src='Images/red.png'></a>";
                }

                row["Edit"] = "<a href=\"Editpage.aspx?pageid=" + row["ID"] + "\"><span class='btn btn-success'>Edit</span></a>";
                row["Delete"] = "<a href=\"Deletepage.aspx?pageid=" + row["ID"] + "\"><span class='btn btn-danger'>Delete</span></a>";

            }

            mytbl.Columns.Remove("page_content1");
            mytbl.Columns.Remove("page_content2");
            mytbl.Columns.Remove("is_deleted");

            myview = mytbl.DefaultView;

            return myview;
        }

        protected void AddPage(object sender, EventArgs e)
        {
            SitePage newPage = new SitePage();
            newPage.PageAuthor = pageAuthor.Text.ToString();
            newPage.PageTitle = pageTitle.Text.ToString();
            newPage.PageContent1 = pageContent1.Text.ToString();
            newPage.PageContent2 = pageContent2.Text.ToString();
            newPage.PageStatus = publishStatus.SelectedValue.ToString();
            insertNewPageQuery += "('" + newPage.PageTitle + "','" + newPage.PageAuthor + "','" + newPage.PageContent1 + "','" + newPage.PageContent2 + "','" + newPage.PageStatus + "',GETDATE(),GETDATE(),0)";
            page_query.InsertCommand = insertNewPageQuery;
            page_query.Insert();
        }
    }
}