﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Editpage.aspx.cs" Inherits="CMS.Editpage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <asp:SqlDataSource runat="server" id="page_query" ConnectionString="<%$ ConnectionStrings:cms_sql_con %>">
    </asp:SqlDataSource>
    <h1 id="heading" runat="server"></h1>
    <div>
        <asp:Label runat="server">Page Title</asp:Label>
        <asp:TextBox runat="server" CssClass="form-control" ID="pageTitle" placeholder="Page Title"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter Page Title" ControlToValidate="pageTitle" ID="validatorPageTitle"></asp:RequiredFieldValidator>
        <br/>
        <asp:Label runat="server">Author Name</asp:Label>
        <asp:TextBox runat="server" CssClass="form-control" ID="pageAuthor" placeholder="Page Author"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter Page Author" ControlToValidate="pageAuthor" ID="validatorPageAuthor"></asp:RequiredFieldValidator>
        <br/>
        <asp:Label runat="server">Page Content 1</asp:Label>
        <asp:TextBox runat="server" CssClass="form-control" ID="pageContent1" TextMode="multiline" Columns="50" Rows="5" placeholder="Page Content-1"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter Page Content 1" ControlToValidate="pageContent1" ID="validatorPageContent1"></asp:RequiredFieldValidator>
        <br/>
        <asp:Label runat="server">Page Content 2</asp:Label>
        <asp:TextBox runat="server" CssClass="form-control" ID="pageContent2" TextMode="multiline" Columns="50" Rows="5" placeholder="Page Content-2"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter Page Content 2" ControlToValidate="pageContent2" ID="validatorPageContent2"></asp:RequiredFieldValidator>
        <br />
        <asp:Label runat="server">Publish Status</asp:Label>
        <asp:DropDownList runat="server" CssClass="form-control" ID="publishStatus">
        <asp:ListItem Value="P" Text="Published"></asp:ListItem>
        <asp:ListItem Value="U" Text="UnPublished"></asp:ListItem>
        </asp:DropDownList>
                  
        <a href="PageManager.aspx"><span class='btn btn-default'>Back</span></a>
        <asp:Button runat="server" CssClass="btn btn-primary" ID="submitButton" OnClick="EditPage" Text="Update Page"/>
        
    </div>
    <div id="result" runat="server"></div>
</asp:Content>
